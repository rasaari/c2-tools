package Old.C2;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import Old.C2.model.SpriteData;

@SuppressWarnings("serial")
public class SpritePicker extends JPanel implements MouseListener {
	private BufferedImage image;
	
	private short frameWidth, frameHeight;
	private String selectedImage, selectedAnimation;
	private HashMap<String, SpriteData> store;
	
	public SpritePicker() {
		frameWidth = 48;
		frameHeight = 2*48;
		
		this.store = new HashMap<String, SpriteData>();
		
		this.addMouseListener(this);
	}
	
	public void paintComponent(Graphics g) { 
		super.paintComponent(g);
		
		Graphics2D g2 = (Graphics2D)g;
		
		if(image == null) {
			g2.drawString("<- select a spriteset from left.", 20, 20);
			return;
		}
		
		g2.drawImage(this.image,0, 0, null);
		
		SpriteData sd = store.get(this.selectedImage);
		if(sd.animations.get(selectedAnimation) == null)
			return;
		
		for(int i = 0; i < sd.animations.get(this.selectedAnimation).size(); i++) {
			int x = sd.getFrameX(i, this.selectedAnimation) * this.frameWidth;
			int y = sd.getFrameY(i, this.selectedAnimation) * this.frameHeight;

			g2.drawRect(x, y, frameWidth, frameHeight);
			g2.drawString(""+(i+1), x, y + 10);
		}
	}

	public void setImage(String selected) {
		this.selectedImage = selected;
		
		if(this.store.containsKey(selected)) {
			this.image = this.store.get(selected).image;
			this.repaint();
			return;
		}
		
		String pathToFile = ProjectSettings.projectPath + ProjectSettings.spritePath + "/" + selected;
		
		try {
			this.image = ImageIO.read(new File(pathToFile));
			
			SpriteData sd = new SpriteData(selected);
			sd.image = image;
			
			sd.frameWidth = this.frameWidth;
			sd.frameHeight = this.frameHeight;
			sd.rows = this.image.getHeight() / this.frameHeight;
			sd.cols = this.image.getWidth() / this.frameWidth;
			
			this.store.put(selected, sd);
			this.repaint();
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}
	
	public void setAnimation(String selected) {
		this.selectedAnimation = selected;
		this.repaint();
		
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		if(this.image == null)
			return;
		
		int x = e.getX();
		int y = e.getY();
		
		x -= x % this.frameWidth;
		y -= y % this.frameHeight;
		
		x /= this.frameWidth;
		y /= this.frameHeight;
		
		SpriteData sd = this.store.get(this.selectedImage);
		
		short id = (short) (y*sd.cols + x);
		
		if(sd.animations.get(this.selectedAnimation) == null)
			sd.animations.put(this.selectedAnimation, new ArrayList<Short>());
		
		sd.animations.get(this.selectedAnimation).add(id);
		
		this.repaint();
	}
	
	public void export() {
		SpriteData sd = this.store.get(this.selectedImage);
		sd.export();
	}
	
	public void clear() {
		SpriteData sd = this.store.get(this.selectedImage);
		if(sd.animations.get(this.selectedAnimation) != null) {
			sd.animations.put(this.selectedAnimation, new ArrayList<Short>());
			this.repaint();
		}
	}
	
	public void dropLast() {
		SpriteData sd = this.store.get(this.selectedImage);
		if(sd.animations.get(this.selectedAnimation) != null) {
			ArrayList<Short> animation = sd.animations.get(this.selectedAnimation);
			if(animation.size() > 0) {
				animation.remove(animation.size()-1);
				this.repaint();
			}
		}
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}


}
