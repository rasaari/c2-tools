package Old.C2;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class PathChooser extends JPanel{
	private JFileChooser fileChooser;
	private String directory;
	
	public PathChooser(){
		fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);		//User can only select project path
		
		int returnVal = fileChooser.showOpenDialog(PathChooser.this);
		
		if(returnVal == JFileChooser.APPROVE_OPTION){
			this.directory = fileChooser.getSelectedFile().getAbsolutePath();
			System.out.println(directory);
		}
	}
	
	public String getDirectory(){
		return this.directory;
	}
	
}
