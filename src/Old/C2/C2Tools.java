//C2 toolkit

package Old.C2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FileDialog;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;

import Old.C2.model.Tile;

@SuppressWarnings("serial")
public class C2Tools extends JFrame implements ActionListener {
	//views needed for creating tilemaps
	TileGrid tileGrid;
	TilePicker tilePicker;
	
	//"Quantum" object for tilemap creation
	Tile activeTile;
	
	//tileset loader and splitter
	ImageSplitter imageChooser;
	BufferedImage images[];
	
	JPanel mapEditor;
	ScriptEditor scriptEditor;
	AnimationEditor animationEditor;
	
	JPanel statusBar;
	JMenuBar menuBar;
	JTabbedPane tabs;
	
	ExecutableActionListener exeListener;
	
	//whenever you need to use tilesize, use this
	public static final int gridSize = 48;
	
	public C2Tools() {
		try {
			// Set System L&F
			UIManager.setLookAndFeel(
			UIManager.getSystemLookAndFeelClassName());
		} catch(Exception e) {
			
		}
		
		Preferences preferences = Preferences.userNodeForPackage( getClass() );
		
		String path = preferences.get("projectPath", System.getProperty("user.dir"));
		ProjectSettings.projectPath = path;
		
		File dataFolder = new File(path+"/data");
		
		if(!dataFolder.exists()) {
			JOptionPane.showMessageDialog(null, "Datafolder not found. Check project path.");
			new ProjectSettings(this).setVisible(true);
			
			File dataFolder2 = new File(ProjectSettings.projectPath+"/data");
			if(!dataFolder2.exists()) {
				JOptionPane.showMessageDialog(null, "No project found in path. Terminating application.");
				System.exit(0);
			}
		}
		
		this.setTitle("C2 Tools");
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.activeTile = new Tile();
		this.exeListener = new ExecutableActionListener();
		
		this.setLayout(new BorderLayout());
		this.initGui();
		
		this.tileGrid.activeTile = this.tilePicker.activeTile = this.activeTile;
		
		this.setVisible(true);
		this.tileGrid.setOffset(new Point(this.getWidth()/2, this.getHeight()/2));
	}

	private void initGui() {
		//TODO: this whole method is a mess. too hard to read. make it better.
		this.tileGrid = new TileGrid(C2Tools.gridSize, activeTile);
		this.tilePicker = new TilePicker(C2Tools.gridSize, activeTile);
		this.tilePicker.setBorder(BorderFactory.createTitledBorder("Tilesets"));
		
		
		this.mapEditor = new JPanel(new BorderLayout());
		this.scriptEditor = new ScriptEditor();
		this.animationEditor = new AnimationEditor();
		
		this.tabs = new JTabbedPane();
		
		this.menuBar = new JMenuBar();
		this.setJMenuBar(this.menuBar);
		
		//C2-menu a.k.a file-menu
		JMenu appMenu = new JMenu("C2 Tools");
			JMenuItem addImgMenuItem = new JMenuItem("Add Image");
			JMenuItem exitMenuItem = new JMenuItem("Exit");
			exitMenuItem.addActionListener(this);
			addImgMenuItem.addActionListener(this);
			appMenu.add(addImgMenuItem);
			appMenu.add(exitMenuItem);
		
		this.menuBar.add(appMenu);
		
		//map specific menu
		JMenu mapMenu = new JMenu("map");
			JMenuItem newMapMenuItem = new JMenuItem("new");
			newMapMenuItem.addActionListener(this);
		
			JMenuItem saveMapMenuItem = new JMenuItem("export map");
			saveMapMenuItem.addActionListener(this);
		
			JMenuItem loadMapMenuItem = new JMenuItem("import map");
			loadMapMenuItem.addActionListener(this);
			
			JMenu runMapMenu = new JMenu("run");
				JMenuItem winRunMapMenuItem = new JMenuItem("windows");
				winRunMapMenuItem.addActionListener(this);
				
				JMenuItem linux32RunMapMenuItem = new JMenuItem("Linux 32");
				linux32RunMapMenuItem.addActionListener(this);
				
				JMenuItem linux64RunMapMenuItem = new JMenuItem("Linux 64");
				linux64RunMapMenuItem.addActionListener(this);
				
				runMapMenu.add(winRunMapMenuItem);
				runMapMenu.add(linux32RunMapMenuItem);
				runMapMenu.add(linux64RunMapMenuItem);
			
			mapMenu.add(newMapMenuItem);
			mapMenu.add(loadMapMenuItem);
			mapMenu.add(saveMapMenuItem);
			mapMenu.add(new JSeparator());
			mapMenu.add(runMapMenu);
			
		this.menuBar.add(mapMenu);
		
		//project specific menu
		JMenu projectMenu = new JMenu("project");
			JMenuItem settingsProjectMenuItem = new JMenuItem("settings");
			settingsProjectMenuItem.addActionListener(this);
			
			JMenu runProjectMenu = new JMenu("run");
				JMenuItem runWindowsMenuItem = new JMenuItem("windows");
				runWindowsMenuItem.addActionListener(this.exeListener);
				
				JMenuItem runLinux32MenuItem = new JMenuItem("Linux 32");
				runLinux32MenuItem.addActionListener(this.exeListener);
				
				JMenuItem runLinux64MenuItem = new JMenuItem("Linux 64");
				runLinux64MenuItem.addActionListener(this.exeListener);
				
				runProjectMenu.add(runWindowsMenuItem);
				runProjectMenu.add(runLinux32MenuItem);
				runProjectMenu.add(runLinux64MenuItem);
			
			projectMenu.add(runProjectMenu);
			projectMenu.add(settingsProjectMenuItem);
		this.menuBar.add(projectMenu);
		
		this.statusBar = new JPanel();
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		this.statusBar.add(new JLabel("mousebutton 1 = add, mousebutton 2 = clear. Drag mousebutton2 with shift to move grid."));
		
		JScrollPane scroll = new JScrollPane(
				tilePicker, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		
		JSplitPane splitter = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, scroll, this.tileGrid);
		splitter.setDividerLocation(4*C2Tools.gridSize+3);
		
		mapEditor.add(splitter, BorderLayout.CENTER);
		
		this.tabs.addTab("Map editor", this.mapEditor);
		this.tabs.addTab("Animations", this.animationEditor);
		this.tabs.addTab("Script editor", this.scriptEditor);
		
		this.add(tabs, BorderLayout.CENTER);
		this.add(statusBar, BorderLayout.SOUTH);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new C2Tools();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			JMenuItem s = (JMenuItem)e.getSource();
			
			//add new tileset image to tilepicker
			if(s.getText() == "Add Image"){
				FileDialog fd = new FileDialog(this, "Select png imagefile", FileDialog.LOAD);
				fd.setFile("*.png");
				fd.setVisible(true);
				String pathToFile = fd.getDirectory() + fd.getFile();
				
				if (fd.getFile() == null) {
					System.out.println("You cancelled the choice");
					return;
				}
				
				this.tileGrid.tilemap.tilesets.add(fd.getFile().substring(0, fd.getFile().length()-4));
				BufferedImage images[] = ImageSplitter.split(pathToFile);
				
				for(BufferedImage image : images) {
					Tile t = new Tile();
					t.image = image;
					this.tilePicker.addTile(t);
				}
			}
			
			//export tilemap from tilegrid to a binary file
			if(s.getText() == "export map") {
				FileDialog fd = new FileDialog(this, "Save tilemap to..", FileDialog.SAVE);
				
				fd.setFile("*.c2map");
				fd.setVisible(true);
				String filename = fd.getDirectory() + fd.getFile();
				
				//check if used cancelled the action
				if(fd.getFile() == null)
					return;
					
				this.tileGrid.tilemap.exportBinary(filename);
			}
			
			if(s.getText() == "import map") {
				FileDialog fd = new FileDialog(this, "Load tilemap from..", FileDialog.LOAD);
				fd.setFile("*.c2map");
				fd.setVisible(true);
				String filename = fd.getDirectory() + fd.getFile();
				
				//check if used cancelled the action
				if(fd.getFile() == null)
					return;
				
				this.tileGrid.tilemap.importBinary(filename);
				
				//load tilesets
				//fail is true if one of the files is not found
				boolean fail = false;
				
				this.tilePicker.tiles.clear();
				this.tilePicker.repaint();
				
				for(String tileset : this.tileGrid.tilemap.tilesets) {
					String pathToFile = ProjectSettings.projectPath + ProjectSettings.tilesetPath + "/" +tileset + ".png";
					File file = new File(pathToFile);
					
					if(file.exists()) {
						BufferedImage images[] = ImageSplitter.split(pathToFile);
						
						for(BufferedImage image : images) {
							Tile t = new Tile();
							t.image = image;
							this.tilePicker.addTile(t);
						}
					}
					
					else {
						fail = true;
						System.err.println("File " + pathToFile + " does not exist!");
					}
				}
				
				//assign tileimages to tilegrid
				if(!fail)
					this.tileGrid.tilemap.assignGraphics(this.tilePicker);
			}
			
			if(s.getText() == "windows") {
				this.tileGrid.tilemap.exportBinary(ProjectSettings.projectPath+"/data/maps/_temp.c2map");
				new Run(Run.WINDOWS, "--map _temp");
				new File(ProjectSettings.projectPath+"/data/maps/_temp.c2map").delete();
			}
			
			if(s.getText() == "Linux 32") {
				this.tileGrid.tilemap.exportBinary(ProjectSettings.projectPath+"/data/maps/_temp.c2map");
				new Run(Run.LINUX32, "--map _temp");
				new File(ProjectSettings.projectPath+"/data/maps/_temp.c2map").delete();
			}
			
			if(s.getText() == "Linux 64") {
				this.tileGrid.tilemap.exportBinary(ProjectSettings.projectPath+"/data/maps/_temp.c2map");
				new Run(Run.LINUX64, "--map _temp");
				new File(ProjectSettings.projectPath+"/data/maps/_temp.c2map").delete();
			}
			
			
			if(s.getText() == "Exit") {
				System.exit(0);
			}
			
			//new map. make this better. There can be other
			//new-menus too..
			if(s.getText() == "new") {
				this.tileGrid.reset();
			}
			
			//project settings dialog
			if(s.getText() == "settings") {
				new ProjectSettings(this).setVisible(true);
			}
		}
	}

}
