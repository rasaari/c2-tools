package Old.C2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class ProjectSettings extends JDialog implements ActionListener {
	public static String projectPath;
	public static String graphicsPath = "/data/gfx";
	public static String tilesetPath = ProjectSettings.graphicsPath + "/tilesets";
	public static String spritePath = ProjectSettings.graphicsPath + "/sprites";
	public static String imagePath = ProjectSettings.graphicsPath + "/images";
	
	private JTextField pathText;
	
	private PathChooser pathChooser;
	
	public ProjectSettings(JFrame parent) {
		super(parent, true);
		
		this.setTitle("Project settings");
		
		this.setModalityType(ModalityType.APPLICATION_MODAL);

		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(350, 200);
		this.setResizable(false);
		
		JPanel projectPathPanel = new JPanel();
		JLabel pathLabel = new JLabel("Project path");
		projectPathPanel.add(pathLabel);
		
		this.pathText = new JTextField(25);
		this.pathText.setText(ProjectSettings.projectPath);
		projectPathPanel.add(this.pathText);
		
		JButton browseButton = new JButton("Browse");
		browseButton.addActionListener(this);
		projectPathPanel.add(browseButton);
		
		JButton okButton = new JButton("ok");
		okButton.addActionListener(this);
		
		JPanel dialogActions = new JPanel();
		dialogActions.add(okButton);	
		
		projectPathPanel.add(dialogActions);
		this.add(projectPathPanel);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton) {
			JButton s = (JButton)e.getSource();
			
			if(s.getText() == "ok") {
				ProjectSettings.projectPath = this.pathText.getText();
				
				Preferences preferences = Preferences.userNodeForPackage( getClass() );
				preferences.put("projectPath", ProjectSettings.projectPath);
				
				this.setVisible(false);
				this.dispose();
			}
			
			if(s.getText() == "Browse"){
				pathChooser = new PathChooser();
				pathText.setText(pathChooser.getDirectory());
			}
		}
			
	}
}
