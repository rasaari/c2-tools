package Old.C2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Old.C2.model.Tile;

public class TilePicker extends JPanel implements MouseListener {
	private int gridSize;
	private int selectedID;
	
	public Vector<Tile> tiles;
	
	private int cols, rows;
	
	public Tile activeTile;
	
	public TilePicker() {
		this.tiles = new Vector<Tile>();
		
		//Tile t = new Tile();
			
		//tiles.add(t);
		
		this.gridSize = 48;
		this.addMouseListener(this);
	}
	
	public TilePicker(int gridSize) {
		this();
		this.gridSize = gridSize;
	}
	
	public TilePicker(int gridSize, Tile activeTile) {
		this(gridSize);
		this.activeTile = activeTile;
		
		if(this.tiles.size() > 0)
			this.activeTile.color = this.tiles.get(selectedID).color;
	}

	public void addTile(Tile t) {
		//get current final id
		t.id = this.makeId();
		
		this.tiles.add(t);
		this.repaint();
	}
	
	private short makeId() {
		if(this.tiles.size() > 0)
			return (short) (this.tiles.lastElement().id + 1);
		else
			return 0;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		int x = 0, y = 0;
		
		this.cols = 0;
		for(int i = 0; i < this.tiles.size(); i++) {
			Tile current = this.tiles.get(i);

			
			if(x > this.getWidth()-this.gridSize) {
				x = 0;
				y += this.gridSize;
			}
			
			//g.setColor(new Color(current.color, current.color, current.color));
			//g.fillRect(x, y, this.gridSize, this.gridSize);
			g.drawImage(current.image, x, y, null);
			
			if(current.isWall) {
				g.setColor(Color.black);
				g.drawString("w", x+2, y+15);
				
				g.setColor(Color.white);
				g.drawString("w", x+1, y+14);
			}
			else if(current.isFloor) {
				g.setColor(Color.black);
				g.drawString("f", x+2, y+15);
				
				g.setColor(Color.white);
				g.drawString("f", x+1, y+14);
			}
				
			if(i == selectedID) {
				g.setColor(Color.white);
				g.drawRect(x, y, this.gridSize-1, this.gridSize-1);
			}
			
			x += 48;
			
			if(this.cols < x) this.cols = x;
		}
		
		this.rows = y/this.gridSize + 1;
		this.cols /= this.gridSize;
		
		this.setPreferredSize(new Dimension(this.gridSize, y + this.gridSize));
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
		boolean left, middle, right;
		left = middle = right = false;
		
		if(e.getButton() == 1)
			left = true;
		
		if(e.getButton() == 2)
			middle = true;
		
		if(e.getButton() == 3)
			right = true;
		
		int x = (e.getX() - e.getX() % this.gridSize) / this.gridSize;
		int y = (e.getY() - e.getY() % this.gridSize) / this.gridSize;
	
		if(y > this.rows - 1 || x > this.cols -1) return;
		
		int id = y * this.cols + x;
		
		if(left) {
		
			this.selectedID = id;
	
			this.activeTile.color = this.tiles.get(selectedID).color;
			this.activeTile.image = this.tiles.get(selectedID).image;
			this.activeTile.id = this.tiles.get(selectedID).id;
			this.activeTile.isFloor = this.tiles.get(selectedID).isFloor;
			this.activeTile.isWall = this.tiles.get(selectedID).isWall;
		}
		
		else if(right) {
			new TileSettings(this.tiles.get(id)).setVisible(true);
		}
		this.repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
