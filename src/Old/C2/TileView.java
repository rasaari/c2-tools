package Old.C2;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JPanel;

/**
 * TileView is a visual component which displays a tilemap and also provides
 * required actions to edit it.
 * @author Jari Saaranen
 *
 */
public class TileView extends JPanel implements MouseListener, MouseMotionListener {
	private static final long serialVersionUID = 1L;
	private int gridSize;
	private Point offset;
	private Point mouse;
	
	private boolean left, right, middle;
	
	//position where the dragging of the mouse starts
	private Point dragStart;
	
	/**
	 * default constructor
	 */
	public TileView() {
		gridSize = 48;
		offset = new Point();
		dragStart = new Point();
		mouse = new Point();
		
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	public void paintComponent(Graphics g) {
		//call parents method first
		super.paintComponent(g);
		
		//start using Graphics2D
		Graphics2D g2 = (Graphics2D)g;
		
		//draw the overlaying grid
		this.drawGrid(g2);
	}
	
	public void drawGrid(Graphics2D g) {
		//color for grid lines
			g.setColor(Color.black);
			
			//stroketype for grid lines
			float[] dash1 = { 2f, 0f, 2f };
			BasicStroke bs1 = new BasicStroke(1, BasicStroke.CAP_BUTT,
	                BasicStroke.JOIN_ROUND, 1.0f, dash1, 2f );

			g.setStroke(bs1);
			
			Point tempOffset = (Point)this.offset.clone();
			
			while(tempOffset.x > this.gridSize || tempOffset.y > this.gridSize) {
				if(tempOffset.x > this.gridSize)
					tempOffset.x -= this.gridSize;
				
				if(tempOffset.y > this.gridSize)
					tempOffset.y -= this.gridSize;
			}
			
			while(tempOffset.x < 0 || tempOffset.y < 0) {
				if(tempOffset.x < 0)
					tempOffset.x += this.gridSize;
				
				if(tempOffset.y < 0)
					tempOffset.y += this.gridSize;
			}
			
			//vertical lines
			for(int y = 0; y < this.getHeight(); y++) {
				if(y % this.gridSize == 0)
					g.drawLine(0, y + tempOffset.y, this.getWidth(), y + tempOffset.y);
			}
			
			//horizontal lines
			for(int x = 0; x < this.getWidth(); x++) {
				if(x % this.gridSize == 0)
					g.drawLine(x + tempOffset.x, 0, x + tempOffset.x, this.getHeight());
			}
			
			//display the origin. this may be removed later
			g.drawString("origin", offset.x, offset.y);
			
			//draw current grid cursor location
			g.setColor(Color.red);
			g.drawRect(mouse.x + tempOffset.x, mouse.y + tempOffset.y, gridSize, gridSize);
	}

	public void setOffset(Point point) {
		this.offset = point;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {
		this.dragStart = e.getPoint();
		
		if(e.getButton() == 1)
			this.left = true;
		
		if(e.getButton() == 2)
			this.middle = true;
		
		if(e.getButton() == 3)
			this.right = true;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(e.getButton() == 1)
			this.left = false;
		
		if(e.getButton() == 2)
			this.middle = false;
		
		if(e.getButton() == 3)
			this.right = false;
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(e.isShiftDown() && this.right) {
			this.offset.x -= this.dragStart.x - e.getX();
			this.offset.y -= this.dragStart.y - e.getY();
			this.repaint();
		
			this.dragStart = e.getPoint();
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.repaint();
		mouse = e.getPoint();
		
		mouse.x -= mouse.x % gridSize;
		mouse.y -= mouse.y % gridSize;
	}

}
