package Old.C2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.swing.JMenuItem;

public class ExecutableActionListener implements ActionListener {
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JMenuItem) {
			JMenuItem s = (JMenuItem)e.getSource();
		
			if(s.getText() == "windows") {
				this.run("C2-win_32.exe");
			}
			
			else if(s.getText() == "Linux 32") {
				this.run("C2-linux_32");
			}
			
			else if(s.getText() == "Linux 64") {
				this.run("C2-linux_64");
			}
		}
	}
	
	public void run(String binary) {
		File file = new File(ProjectSettings.projectPath + "/" + binary);
		
		if(!file.exists()) {
			return;
		}
		
		try {
			Process process = new ProcessBuilder(ProjectSettings.projectPath + "/" + binary).start();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;

			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
