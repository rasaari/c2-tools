package Old.C2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class AnimationEditor extends JPanel 
implements ActionListener, MouseListener {
	JPanel detectedSpriteSheets;
	JList<String> spriteList;
	SpritePicker spritePicker;
	
	JPanel buttons;
	JPanel animations;
	JList<String> animationList;
	
	DefaultListModel<String> filenames;
	
	public AnimationEditor() {
		this.setLayout(new BorderLayout());
		
		this.detectedSpriteSheets = new JPanel();
		this.detectedSpriteSheets.setBorder(BorderFactory.createTitledBorder("Sprite Sheets"));
		
		this.animations = new JPanel();
		this.animations.setBorder(BorderFactory.createTitledBorder("Sprites"));
		
		this.filenames = new DefaultListModel<String>();
		this.buttons = new JPanel();
		
		this.spritePicker = new SpritePicker();
		
		JButton exportButton = new JButton("export");
		exportButton.addActionListener(this);
		
		JButton clearButton = new JButton("clear animation");
		clearButton.addActionListener(this);
		
		JButton removeLastButton = new JButton("remove last frame");
		removeLastButton.addActionListener(this);
		this.buttons.add(exportButton);
		this.buttons.add(clearButton);
		this.buttons.add(removeLastButton);
		
		String[] animationNames = {
			"walk_0",
			"walk_45",
			"walk_90",
			"walk_135",
			"walk_180",
			"walk_225",
			"walk_270",
			"walk_315",
			"idle_0",
			"idle_45",
			"idle_90",
			"idle_135",
			"idle_180",
			"idle_225",
			"idle_270",
			"idle_315",
			"use_0",
			"use_45",
			"use_90",
			"use_135",
			"use_180",
			"use_225",
			"use_270",
			"use_315",
			"die",
			"dead"
		};
		
		JScrollPane spriteScroll = new JScrollPane(
				this.detectedSpriteSheets, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		spriteScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		this.spriteList = new JList<String>(this.filenames);
		this.spriteList.setFixedCellWidth(150);
		this.spriteList.addMouseListener(this);
		this.spriteList.setName("spriteList");
		this.detectedSpriteSheets.add(this.spriteList);
		
		JScrollPane animationScroll = new JScrollPane(
				this.animations, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		animationScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		this.animationList = new JList<String>(animationNames);
		this.animationList.setFixedCellWidth(100);
		this.animationList.addMouseListener(this);
		this.animationList.setName("animationList");
		this.animations.add(this.animationList);
		
		this.add(buttons, BorderLayout.NORTH);
		
		this.add(spriteScroll, BorderLayout.WEST);
		this.add(animationScroll, BorderLayout.EAST);
		this.add(this.spritePicker, BorderLayout.CENTER);
		
		this.spritePicker.setAnimation(animationNames[0]);
		
		this.loadSprites();
	}

	public void loadSprites() {
		String pathToSprites = ProjectSettings.projectPath + ProjectSettings.spritePath;
		
		File folder = new File(pathToSprites);
		System.out.println(pathToSprites);
		File[] files = folder.listFiles();
		
		for(File f: files) {
			if(f.isFile() && f.getName().endsWith(".png")) {
				this.filenames.addElement(f.getName());
			}
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton s = (JButton)e.getSource();
		
		if(s.getText() == "export") {
			this.spritePicker.export();
		}
		
		else if(s.getText() == "clear animation") {
			this.spritePicker.clear();
		}
		
		else if(s.getText() == "remove last frame") {
			this.spritePicker.dropLast();
		}
		
	}

	@Override// TODO Auto-generated method stub
	public void mouseClicked(MouseEvent e) {
		JList s = (JList)e.getSource();
		
		if(s.getName() == "spriteList") {
			String selected = (String)s.getSelectedValue();
			this.spritePicker.setImage(selected);
		}
		
		else if(s.getName() == "animationList") {
			String selected = (String)s.getSelectedValue();
			this.spritePicker.setAnimation(selected);
		}
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
