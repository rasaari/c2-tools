package Old.C2;

import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Old.C2.model.Tile;

public class TileSettings extends JDialog implements ActionListener {
	JCheckBox wall, floor;
	Tile tile;
	
	public TileSettings(Tile etile) {
		this.tile = etile;
		
		this.setTitle("Tile settings");
		
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(350, 200);
		this.setResizable(false);
		
		System.out.println("Editing: " + tile);
		
		JPanel editPanel = new JPanel();
		
		wall = new JCheckBox("is wall");
		wall.addActionListener(this);
		
		if(tile.isWall)
			wall.setSelected(true);
		
		floor = new JCheckBox("is floor");
		floor.addActionListener(this);
		
		if(tile.isFloor)
			floor.setSelected(true);
		
		editPanel.add(wall);
		editPanel.add(floor);
		
		this.add(editPanel);
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(wall.isSelected())
			floor.setSelected(false);
		
		else if(floor.isSelected())
			wall.setSelected(false);
		
		tile.isWall = wall.isSelected();
		tile.isFloor = floor.isSelected();
	}

}
