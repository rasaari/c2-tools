package Old.C2;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

public class ImageSplitter {
	public static BufferedImage[] split(String pathToFile){
		BufferedImage image;
		
		try {
			image = ImageIO.read(new File(pathToFile));
		} catch (IOException e){
			e.printStackTrace();
			return null;
		}

		if (
				image.getHeight() % C2Tools.gridSize != 0 || 
				image.getWidth() % C2Tools.gridSize != 0){
			//TODO: show errormessage
			
			//do not continue.
			return null;
		}
		
		int rows = image.getHeight() / C2Tools.gridSize;
		int cols = image.getWidth() / C2Tools.gridSize;
		int chunks = rows * cols;
		
		int count = 0;
		
		BufferedImage images[] = new BufferedImage[chunks];
		
		for(int y = 0; y < rows; y++){
			for(int x = 0; x < cols; x++){
				images[count] = new BufferedImage(
						C2Tools.gridSize, 
						C2Tools.gridSize, 
						image.getType());
				
				// draws the image chunk  
                Graphics2D gr = images[count++].createGraphics();  
                gr.drawImage(
                		image, 
                		0, 
                		0, 
                		C2Tools.gridSize, 
                		C2Tools.gridSize, 
                		C2Tools.gridSize * x, 
                		C2Tools.gridSize * y, 
                		C2Tools.gridSize * x + C2Tools.gridSize, 
                		C2Tools.gridSize * y + C2Tools.gridSize, 
                		null);  
                gr.dispose();
			}
		}
		
		return images;
	}
}