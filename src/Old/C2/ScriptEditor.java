package Old.C2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

@SuppressWarnings("serial")
public class ScriptEditor extends JPanel implements ActionListener, MouseListener{
	JPanel scriptPicker,scriptViewer;
	JList<String> scriptList;
	
	public ScriptEditor(){
		this.setLayout(new BorderLayout());
		
		this.scriptPicker = new JPanel();
		this.scriptPicker.setPreferredSize(new Dimension(200,200));
		this.scriptPicker.setBorder(BorderFactory.createTitledBorder("Scripts"));
		
		this.scriptViewer = new JPanel();
		this.scriptViewer.setPreferredSize(new Dimension(570,200));
		this.scriptViewer.setBorder(BorderFactory.createTitledBorder("ScriptViewer")); //Probably better without title
		
		JScrollPane scriptScroll = new JScrollPane(
				this.scriptPicker, 
				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scriptScroll.getVerticalScrollBar().setUnitIncrement(16);
		
		//Adding scripts to a JList?
		/*this.scriptList = new JList<String>();
		this.scriptList.setFixedCellWidth(100);
		this.scriptList.addMouseListener(this);
		this.scriptList.setName("scriptList");
		this.scriptPicker.add(this.scriptList);*/
		
		this.add(scriptScroll, BorderLayout.WEST);
		this.add(scriptViewer,BorderLayout.EAST);
	}
	
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
