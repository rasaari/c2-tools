package Old.C2.collection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Vector;

import Old.C2.C2Tools;
import Old.C2.TilePicker;
import Old.C2.model.Tile;

@SuppressWarnings("serial")
public class Tilemap<T> extends Vector<T> {
	//tilesets used in this map
	public Vector<String> tilesets;
	
	public static final int NewTile = 1;
	public static final int ChangeLayer = 2;
	
	//ingame tilemap properties
	public byte world;
	public byte scene;
	public String name;
	public byte tileSize;
	
	public Tilemap() {
		super();
		this.tilesets = new Vector<String>();
	}
	
	public void exportBinary(String path) {
		System.out.println("Exporting tilemap to file: "+path);
		System.out.println("tilemap size: "+this.size());
		
		try {
			DataOutputStream os = new DataOutputStream(new FileOutputStream(path));
			
			//-- tilemap header --
			
			//world id
			os.writeByte(0);
			
			//scene id
			os.writeByte(0);
			
			//tilesize
			os.writeByte(C2Tools.gridSize);
			
			os.writeBytes("C2 map from editor\0");
			
			//tilesets
			for(int i = 0; i < tilesets.size(); i++) {
				String tileset = tilesets.get(i);
				os.writeBytes(tileset+"\0");
			}
			
			//end of header
			os.writeByte(0);
			
			//-- tilemap body --
			
			//loop through every layer and write tiles to file
			//from bottom to top
			for(byte layer = 0; layer < 6; layer++) {
				os.writeByte(Tilemap.ChangeLayer);
				os.writeByte(layer);
				
				for(int i = 0; i < this.size(); i++) {
					Tile t = (Tile) this.get(i);
					if(t.z == layer) {
						t.writeBinary(os);
					}
				}
			}
			
			os.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	@SuppressWarnings("unchecked")
	public void importBinary(String path) {
		try {
			DataInputStream is = new DataInputStream(new FileInputStream(path));
			
			this.clear();
			this.tilesets.clear();
			
			//-- tilemap header --
			this.world = is.readByte();
			this.scene = is.readByte();
			this.tileSize = is.readByte();
			
			this.name = "";
			
			//read map's name
			while(is.available() > 0) {
				char c = (char)is.read();
				if(c == '\0')
					break;
				
				this.name += c;
			}
			
			String tileset = "";
			
			//read names of the tilesets
			while(is.available() > 0) {
				char c = (char)is.read();
				
				//if found terminator character
				if(c == '\0') {
					this.tilesets.add(tileset);
					tileset = "";
					
					//read nex character also..
					c = (char)is.read();
					
					//and if that is terminator too, all tilesets are read
					if(c == '\0')
						break;
					
					
				}
				
				tileset += c;
			}
			
			//-- tilemap body --
			
			byte cmd = '\0';
			byte layer = 0;
			
			int x, y;
			short id;
			byte wallOrFloor;
			Tile t;
			while(is.available() > 0) {
				cmd = is.readByte();
				
				//tile
				if(cmd == 1) {
					x = Integer.reverseBytes(is.readInt());
					y = Integer.reverseBytes(is.readInt());
					id = Short.reverseBytes(is.readShort());
					wallOrFloor = is.readByte();
					
					
					t = new Tile();
					t.x = x;
					t.y = y;
					t.id = id;
					t.z = layer;
					
					if(wallOrFloor == 0x1)
						t.isWall = true;
					else if(wallOrFloor == 0x2)
						t.isFloor = true;
					
					this.add((T) t);
					
				}
				
				if(cmd == 2) {
					layer = is.readByte();
				}
			}
			
			is.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void assignGraphics(TilePicker tilePicker) {
		if(tilePicker.tiles.size() == 0)
			return;
		
		for(int i = 0; i < this.size(); i++) {
			Tile t = (Tile) this.get(i);
			
			t.image = tilePicker.tiles.get(t.id).image;
		}
	}
}
