package Old.C2;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Run {

	public static final String WINDOWS = "C2-win_32.exe";
	public static final String LINUX32 = "C2-linux_32";
	public static final String LINUX64 = "C2-linux_64";
	
	public Run(String target, String params) {
		File file = new File(ProjectSettings.projectPath + "/" + target);
		
		if(!file.exists()) {
			return;
		}
		
		String []args = params.split(" ");
		List<String> command = new ArrayList<String>();
		
		command.add(ProjectSettings.projectPath + "/" + target);
		
		for(int i = 0; i < args.length; i++) {
			command.add(args[i]);
		}
		
		try {
			Process process = new ProcessBuilder(command).start();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line;

			while ((line = br.readLine()) != null) {
				System.out.println(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
