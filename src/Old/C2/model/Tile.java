package Old.C2.model;

import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;

import Old.C2.collection.Tilemap;

public class Tile {
	public int x, y;
	public byte z;
	
	//graphics id, used by the game-engine
	public short id;
	
	public BufferedImage image;
	public int color;
	
	public boolean isWall;
	public boolean isFloor;
	
	public Tile() {
		color = (int) Math.floor(Math.random()*100)+50;
		this.z = 0;
		isWall = isFloor = false;
	}
	
	public Tile(int x, int y) {
		this();
		this.x = x;
		this.y = y;
	}
	
	public Tile(Tile original) {
		if(original == null) return;
		
		this.x = original.x;
		this.y = original.y;
		this.z = original.z;
		this.id = original.id;
		this.color = original.color;
		this.image = original.image;
		this.isWall = original.isWall;
		this.isFloor = original.isFloor;
	}
	
	public void writeBinary(DataOutputStream os) {
		try {
			os.writeByte(Tilemap.NewTile);
			os.writeInt(Integer.reverseBytes(this.x));
			os.writeInt(Integer.reverseBytes(this.y));
			os.writeShort(Short.reverseBytes(this.id));
			
			if(this.isWall)
				os.writeByte(0x1);
			else if(this.isFloor)
				os.writeByte(0x2);
			else
				os.writeByte(0x0);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
