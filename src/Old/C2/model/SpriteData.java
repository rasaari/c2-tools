package Old.C2.model;

import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import Old.C2.ProjectSettings;

public class SpriteData {
	public BufferedImage image;
	public HashMap<String, ArrayList<Short>> animations;
	public int rows, cols;
	public short frameWidth, frameHeight;
	public String filename;
	
	public SpriteData(String filename) {
		this.animations = new HashMap<String, ArrayList<Short>>();
		this.filename = filename;
	}
	
	public int getFrameX(int frame, String animation) {
		if(!this.animations.containsKey(animation))
			return 0;
		
		ArrayList<Short> anim = this.animations.get(animation);
		return anim.get(frame) % cols;
	}
	
	public int getFrameY(int frame, String animation) {
		if(!this.animations.containsKey(animation))
			return 0;
		
		ArrayList<Short> anim = this.animations.get(animation);
		return (anim.get(frame) - this.getFrameX(frame, animation)) / cols;
	}
	
	public void export() {
		String animFile = this.filename.substring(0, this.filename.length()-4) + ".c2anim";
		String target = ProjectSettings.projectPath + ProjectSettings.spritePath + "/" + animFile;
		
		if(this.animations.isEmpty())
			return;
		
		try {
			DataOutputStream os = new DataOutputStream(new FileOutputStream(target));
			
			os.writeShort(Short.reverseBytes(this.frameWidth));
			os.writeShort(Short.reverseBytes(this.frameHeight));
			
			for(String key : this.animations.keySet()){
				//System.out.println(key.length());
				os.writeBytes(key);
				os.writeByte(0);
				
				os.writeShort(Short.reverseBytes((short) this.animations.get(key).size()));
				
				for(Short frame : this.animations.get(key)) {
					os.writeShort(Short.reverseBytes(frame));
				}
			}
			
			os.close();
			
			//os.writeInt(Integer.reverseBytes(this.frame));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
