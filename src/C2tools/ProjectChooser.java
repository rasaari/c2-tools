package C2tools;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Dialog.ModalityType;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

@SuppressWarnings("serial")
public class ProjectChooser extends JDialog implements ActionListener{
	private String projectPath = "C:/";
	
	private JPanel projectPathPanel;
	private JPanel projectViewPanel;
	private JPanel buttonPanel;
	
	private JButton browseButton;
	private JButton newButton;
	private JButton deleteButton;

	private JTextField projectTextField;
	private JTextArea projectListArea;
	
	
	public ProjectChooser(JFrame frame) {
		super(frame, true);
		initComponents();
		initGUI();
	}

	private void initGUI() {
		 try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e){}
		
		this.add(projectViewPanel, BorderLayout.WEST);
		this.add(buttonPanel, BorderLayout.EAST);
		this.add(projectPathPanel, BorderLayout.SOUTH);

		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setTitle("Open Project");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		
		this.setSize(350, 200);
		this.setResizable(false);
	}

	private void initComponents() {
		//JPanels
		//Panel for choosing project path
		this.projectPathPanel = new JPanel(new BorderLayout());
			this.projectPathPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
			this.projectPathPanel.setBorder(BorderFactory.createTitledBorder("Project Path"));
		
		//Panel for Project List
		this.projectViewPanel = new JPanel(new BorderLayout());
			this.projectViewPanel.setPreferredSize(new Dimension(265, 50));
			this.projectViewPanel.setBorder(BorderFactory.createTitledBorder("Project List"));
		
		//Panel for buttons
		this.buttonPanel = new JPanel();
			this.buttonPanel.setPreferredSize(new Dimension(80,50));
		
		//JTextFields and Areas
		this.projectTextField = new JTextField(28);
			this.projectTextField.setText(this.projectPath);
			
		this.projectListArea = new JTextArea();
			this.projectListArea.setPreferredSize(new Dimension(32,30));
			this.projectListArea.setEditable(false);
			
		//JButtons
		this.browseButton = new JButton("...");
			browseButton.setPreferredSize(new Dimension(20,15));
			
		this.newButton = new JButton("Create");
		this.deleteButton = new JButton("Delete");
		
		//Adding components to panels
		this.projectPathPanel.add(projectTextField, BorderLayout.WEST);
		this.projectPathPanel.add(browseButton, BorderLayout.EAST);
		
		this.buttonPanel.add(newButton, BorderLayout.NORTH);
		this.buttonPanel.add(deleteButton, BorderLayout.SOUTH);
		
		this.projectViewPanel.add(projectListArea);
		
		//ActionListeners
		this.browseButton.addActionListener(this);
		this.newButton.addActionListener(this);
		this.browseButton.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() instanceof JButton){
			JButton b = (JButton)e.getSource();
			
				
			
		}
		
	}
}
