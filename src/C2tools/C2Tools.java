package C2tools;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.BevelBorder;

import C2tools.AnimationEditor.AnimationEditor;
import C2tools.ScriptEditor.ScriptEditor;
import Old.C2.ProjectSettings;

/*Main class*/


@SuppressWarnings("serial")
public class C2Tools extends JFrame implements ActionListener{
	boolean start = true; //Opening Project Dialog first, then C2Tools
	
	//Panels
	private	JPanel mapEditor;
	private	JPanel statusBar;
	
	//Menus
	private JMenu c2Menu, projectMenu;
	private JMenuItem addImgMenuItem;
	private JMenuItem exitMenuItem;
	private JMenuItem settingsProjectMenuItem;
	private	JMenuBar menuBar;
	
	//Tabs
	private	JTabbedPane tabs;
	
	//Objects
	private AnimationEditor animationEditor;
	private ScriptEditor scriptEditor;
	
	public static void main(String[] args) {
		new C2Tools();
	}
	
	public C2Tools(){
		if(start){
			start = false;
			new ProjectChooser(this).setVisible(true);
		}
		
		initComponents();
		initGUI();
	}
	
	//Initializes 
	private void initGUI() {
		this.add(tabs, BorderLayout.CENTER);
		this.add(statusBar, BorderLayout.SOUTH);
		
		this.setTitle("C2 Tools");
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setVisible(true);
	}
	
	//Initialize all components here
	private void initComponents(){		
		//JPanels
		this.mapEditor = new JPanel(new BorderLayout());
		this.statusBar = new JPanel();
			this.statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
			this.statusBar.add(new JLabel("MouseLeft = Add      MouseRight = Clear      Drag MouseRight + Shift to move grid."));
		
		this.animationEditor = new AnimationEditor();
		this.scriptEditor = new ScriptEditor();
		
		//Menus
		//C2Tools Menu
		this.c2Menu = new JMenu("C2Tools");
			this.addImgMenuItem = new JMenuItem("Add Image");
			this.exitMenuItem = new JMenuItem("Exit");
			this.menuBar = new JMenuBar();
			this.setJMenuBar(this.menuBar);
			
			this.c2Menu.add(addImgMenuItem);
			this.c2Menu.add(exitMenuItem);
		
		//Project Menu
		this.projectMenu = new JMenu("Project");
			this.settingsProjectMenuItem = new JMenuItem("Settings");
			this.projectMenu.add(settingsProjectMenuItem);
			
		this.menuBar.add(c2Menu);
		this.menuBar.add(projectMenu);
		
		//Tabs
		this.tabs = new JTabbedPane();
			this.tabs.addTab("Map editor", this.mapEditor);
			this.tabs.addTab("Animations", this.animationEditor);
			this.tabs.addTab("Script editor", this.scriptEditor);
			
		//Dialogs
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	}
}
